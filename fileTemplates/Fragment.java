package ${packageName};

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class ${className} extends Fragment {

    //public static final String arg_param1 = "SOMETHING";

    public static ${className} newInstance()
    {
        ${className} frag = new ${className}();

        // Get arguments pass in, if any
        Bundle args = frag.getArguments();
        if(args == null)
        {
            args = new Bundle();
        }

        // Add parameters to the arguments bundle
        //args.putInt(SOME_KEY, someValue);

        frag.setArguments(args);

        return frag;
    }

    public ${className}()
    {
        // TODO
    }

    //------- Start Fragment Lifecycle -----------
    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {

        return inflater.inflate(R.layout.fragment_${classToResource(className)}, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart()
    {
        super.onStart();
    }

    @Override
    public void onResume()
    {
        super.onResume();
    }

    @Override
    public void onPause()
    {
        super.onPause();
    }

    @Override
    public void onStop()
    {
        super.onStop();
    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
    }
    //------- End Fragment Lifecycle -------------
}