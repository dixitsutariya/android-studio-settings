import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

#parse("File Header.java")
public class ${NAME} extends RecyclerView.Adapter<${NAME}.ViewHolder> {

    private static final String TAG = ${NAME}.class.getSimpleName();
    
    private Context mContext;
    private List<Object> mData;
    
    /**
     * Change {@link List} type according to your needs
     */
    public ${NAME}(Context context, List<Object> data) {
        if(context == null) {
            throw new NullPointerException("context can not be NULL");
        }
        
        if(data == null) {
            throw new NullPointerException("data list can not be NULL");
        }
        
        this.mContext = context;
        this.mData = data;
    }
    
    
    /**
     * Change the null parameter to a layout resource {@code R.layout.example}
     */
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                                    .inflate(null, parent, false);
        
        return new ViewHolder(view);
    }
    
    
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // include binding logic here
    }
    
    
    @Override
    public int getItemCount() {
        return mData.size();
    }
    
    
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // include {@link View} components here
        
        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}